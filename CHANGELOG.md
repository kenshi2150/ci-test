## [1.5.1](https://gitlab.com/kenshi2150/ci-test/compare/v1.5.0...v1.5.1) (2020-11-27)


### Bug Fixes

* release file + package-lock.json ([534e782](https://gitlab.com/kenshi2150/ci-test/commit/534e782d76e9f1bd929c4f1a1d2fe5f79315c98c))

# [1.5.0](https://gitlab.com/kenshi2150/ci-test/compare/v1.4.0...v1.5.0) (2020-11-27)


### Bug Fixes

* config for release ([45973cf](https://gitlab.com/kenshi2150/ci-test/commit/45973cfb26a8e6c86b5e02639a067ae015863b1a))
* enable npm ([32a0ed6](https://gitlab.com/kenshi2150/ci-test/commit/32a0ed6e9a41490d0c0314ec8ad70fde8f8077e5))
* new ([94bbc76](https://gitlab.com/kenshi2150/ci-test/commit/94bbc76a9663b32648c225d12877d00b1fa4798e))


### Features

* 1 ([371facc](https://gitlab.com/kenshi2150/ci-test/commit/371facc92212ebfcd15013a53a0b63881f9046a0))
* 2 ([dfa8086](https://gitlab.com/kenshi2150/ci-test/commit/dfa80869c7111a9ae24bfc162f4b2f59cc214a29))

# [1.4.0](https://gitlab.com/kenshi2150/ci-test/compare/v1.3.2...v1.4.0) (2020-11-26)


### Features

* ... ([33479b5](https://gitlab.com/kenshi2150/ci-test/commit/33479b56931ab340c3aba7ca051fb18d9a041081))
